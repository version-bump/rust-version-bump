use git2::{Commit, Error, Repository};

/// Gathers the commits from the parsed revision, usually from a tag to the current
/// HEAD, but open to anything.
pub fn get_commits_from_spec<'a>(
  repo: &'a Repository,
  spec: &str,
) -> Result<Vec<Commit<'a>>, Error> {
  // Create the revwalk
  let mut revwalk = repo.revwalk()?;
  // revparse this thing
  let revspec = repo.revparse(spec)?;

  // If this has a
  if revspec.mode().contains(git2::RevparseMode::SINGLE) {
    revwalk.push(revspec.from().unwrap().id())?;
  } else {
    let from = revspec.from().unwrap().id();
    let to = revspec.to().unwrap().id();
    revwalk.push(to)?;

    if revspec.mode().contains(git2::RevparseMode::MERGE_BASE) {
      let base = repo.merge_base(from, to)?;
      let o = repo.find_object(base, Some(git2::ObjectType::Commit))?;
      revwalk.push(o.id())?;
    }
    revwalk.hide(from)?;
  }
  let filtered_commits = revwalk.filter_map(|oid| {
    if let Ok(o) = oid {
      if let Ok(commit) = repo.find_commit(o) {
        return Some(commit);
      }
    }
    return None::<Commit<'a>>;
  });

  // what is this absolute clown shoes?
  let ff: Vec<Commit<'a>> = filtered_commits.collect();
  return Ok(ff);
}
