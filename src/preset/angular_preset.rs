// Fair warning: I have no fucking idea what I'm doing in rust.
use crate::preset::Preset;
use semver::Version;
use std::str::FromStr;
pub struct AngularPreset;

impl Preset for AngularPreset {
  fn calculate_version(
    &self,
    current_version: &str,
    _commits: Vec<git2::Commit>,
  ) -> Result<String, git2::Error> {
    let semver_version =
      Version::from_str(current_version).expect("Could not parse current version");
    println!("{:?}", semver_version);
    for commit in _commits {
      let summary = commit.summary().expect("No commit summary");
      println!("{}", summary);
    }
    return Ok(String::from("HELP"));
  }
}
