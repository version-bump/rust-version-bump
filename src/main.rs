/*
 For the git issue:
   option 1: we create our own git library to handle the very basic use cases we need.
   option 2: we figure out how to use the libgit2 library we already have.
 Probably going for: option 2... but if it pisses me off enough we're just going to figure out a way to do it ourselves
   because like, _yeesh_
*/

use git2::Repository;
use version_bump::util::get_commits_from_spec;

fn main() -> std::io::Result<()> {
  let repo = Repository::open(".").expect("ok");
  let commits = get_commits_from_spec(&repo, "0.1.0..HEAD");
  if commits.is_ok() {
    let commits = commits.unwrap();
    for commit in commits {
      println!("{}", commit.summary().expect("fkdfjkdj"));
    }
  }
  return Ok(());
}

/*
 * 1. Determine CWD.
 * 2. Determine (and instantiate) the following
 *    - VersionStrategy
 *    - GitProvider
 *    - Preset
 * 3. Get current version from VersionStrategy
 * 4. Gather commits between current_version and the HEAD.
 * 5. Pass current_verison and Commits to preset to determine new version
 * 6. using VersionStrategy, bump to new_version.
 * 7. Generate CHANGELOG (multiple substeps, document later)
 */
