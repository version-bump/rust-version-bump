use crate::git::GitUrl;
use crate::providers::Provider;
use std::format;
pub struct GitlabProvider {}

impl Provider for GitlabProvider {
  fn commit_url(url: GitUrl, sha: &str) -> String {
    return format!("https://{}/{}/-/commit/{}", url.host, url.path, sha);
  }
  fn get_diff_url(url: GitUrl, from: &str, to: &str) -> String {
    return format!(
      "https://{}/{}/-/compare/{}..{}",
      url.host, url.path, from, to
    );
  }
}
