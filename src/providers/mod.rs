pub mod bitbucket;
pub mod github;
pub mod gitlab;
use crate::git::GitUrl;

pub trait Provider {
  fn get_diff_url(url: GitUrl, from: &str, to: &str) -> String;
  fn commit_url(url: GitUrl, sha: &str) -> String;
}
