use crate::{git::GitUrl, providers::Provider};

pub struct GithubProvider {}
impl Provider for GithubProvider {
  fn commit_url(url: GitUrl, sha: &str) -> String {
    return format!("https://{}/{}/commit/{}", url.host, url.path, sha);
  }
  fn get_diff_url(url: GitUrl, from: &str, to: &str) -> String {
    return format!("https://{}/{}/compare/{}..{}", url.host, url.path, from, to);
  }
}
