pub mod base_changelog_writer;
use git2::Commit;

pub trait ChangelogWriter {
  fn write(&self, file_path: String, new_content: String) -> Result<(), std::io::Error>;
  fn generate_changelog_entry(
    &self,
    new_version: String,
    previous_version: String,
    commits: Vec<Commit>,
  ) -> Result<String, std::io::Error>;
}
