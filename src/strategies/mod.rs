pub mod cargo;
pub mod deno;
pub mod node;
use std::io::Error;

pub trait VersionStrategy {
  fn get_current_version(&self) -> Result<String, Error>;
  fn bump(&self, new_version: String) -> Result<(), Error>;
}
