use crate::strategies::VersionStrategy;
use regex::Regex;
use std::fs::{read_to_string, File};
use std::io;
use std::io::{BufRead, Write};
use std::path::Path;
pub struct DenoVersionStrategy {
  pub cwd: String,
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
  P: AsRef<Path>,
{
  let file = File::open(filename)?;
  Ok(io::BufReader::new(file).lines())
}

impl VersionStrategy for DenoVersionStrategy {
  // Writes the version
  fn bump(&self, new_version: String) -> Result<(), std::io::Error> {
    let regex =
      Regex::new(r#"const VERSION\s?=\s?('|")(?P<version>.*)('|");?"#).expect("regex should parse");
    let file_name = format!("{}/{}", self.cwd, "deps.ts");
    let file_name_str = file_name.as_str();
    let file_contents = read_to_string(file_name_str);
    if let Ok(content) = file_contents {
      let content_str = content.as_str();
      // TODO(jim): Figure this out so that we can have whatever the user prefers here instead of hard coding single quotes.
      let after = regex.replace(
        content_str,
        format!(
          "const VERSION = '{new_version}';",
          new_version = new_version
        )
        .as_str(),
      );
      let mut deps_file = File::create(file_name).expect("Could not open deps.ts file");
      return deps_file.write_all(after.as_bytes());
    }

    return Err(file_contents.err().unwrap());
  }
  fn get_current_version(&self) -> Result<String, std::io::Error> {
    let base_version = "0.1.0";
    let regex =
      Regex::new(r#"const VERSION\s?=\s?('|")(?P<version>.*)('|");?"#).expect("regex should parse");

    // File hosts must exist in current path before this produces output
    if let Ok(lines) = read_lines(format!("{}/{}", self.cwd, "deps.ts")) {
      // Consumes the iterator, returns an (Optional) String
      for line in lines {
        if let Ok(ip) = line {
          let ip_clone = ip.clone();
          let line_match = regex.captures(ip_clone.as_str());
          if line_match.is_some() {
            let version_match = line_match.unwrap().name("version").expect("Wrong");
            return Ok(String::from(version_match.as_str()));
          }
        }
      }
    }
    return Ok(String::from(base_version));
  }
}

mod test {
  use crate::strategies::{deno::DenoVersionStrategy, VersionStrategy};

  #[test]
  pub fn test_get_current_version() {
    // Create the DVS with the correct current directory
    let dvs = DenoVersionStrategy {
      cwd: String::from("./tests/deno"),
    };
    // grab the response
    let resp = dvs
      .get_current_version()
      .expect("Should be able to fetch current version");

    // assert eq
    assert_eq!(resp, String::from("0.1.0"));
  }

  #[test]
  pub fn test_bump_version() {
    let dvs = DenoVersionStrategy {
      cwd: String::from("./tests/deno"),
    };

    let resp = dvs.bump(String::from("1.0.0"));
    // If the response is Ok then it _should_ have written to the source.
    if resp.is_ok() {
      let current_version = dvs
        .get_current_version()
        .expect("expect to get current version");
      assert_eq!(current_version, "1.0.0");
    }
  }
}
