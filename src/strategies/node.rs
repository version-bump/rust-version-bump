use serde_json::Value;
use std::fs::{read_to_string, File};
use std::io::BufRead;
use std::io::{self, Write};
use std::path::Path;

use regex::Regex;

use crate::strategies::VersionStrategy;
pub struct NodeVersionStrategy {
  pub cwd: String,
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
  P: AsRef<Path>,
{
  let file = File::open(filename)?;
  Ok(io::BufReader::new(file).lines())
}

impl VersionStrategy for NodeVersionStrategy {
  fn bump(&self, new_version: String) -> Result<(), std::io::Error> {
    let file_name = format!("{}/{}", self.cwd, "package.json");
    let file_path = Path::new(file_name.as_str());
    let file_contents = read_to_string(file_path);
    if let Ok(c) = file_contents {
      let mut json_content: Value = serde_json::from_str(c.as_str())?;
      json_content["version"] = Value::String(new_version);
      let mut package_file = File::create(file_path).expect("need file");
      return package_file.write_all(
        serde_json::to_string_pretty(&json_content)
          .unwrap()
          .as_bytes(),
      );
    }
    return Ok(());
  }

  fn get_current_version(&self) -> Result<String, std::io::Error> {
    let fs_string = format!("{}/{}", self.cwd, "package.json");
    let file_name = Path::new(fs_string.as_str());
    let reg =
      Regex::new(r#""version": "(?P<version>\d+\.\d+\..*)""#).expect("Could not build regex");

    if let Ok(lines) = read_lines(file_name) {
      for line in lines {
        if let Ok(ip) = line {
          let ip_clone = ip.clone();
          if let Some(cap) = reg.captures(ip_clone.as_str()) {
            if let Some(version) = cap.name("version") {
              return Ok(String::from(version.as_str()));
            }
          }
        }
      }
    }

    return Ok(String::from("0.1.0"));
  }
}

mod test {
  use crate::strategies::{node::NodeVersionStrategy, VersionStrategy};
  #[test]
  pub fn test_get_current_verison() {
    let nvs = NodeVersionStrategy {
      cwd: String::from("./tests/node"),
    };
    if let Ok(current_version) = nvs.get_current_version() {
      assert_eq!(current_version, String::from("0.1.0"));
    }
  }
  #[test]
  pub fn test_zzzzzzz() {
    let nvs = NodeVersionStrategy {
      cwd: String::from("./tests/node"),
    };

    let resp = nvs.bump(String::from("1.0.0"));
    if resp.is_ok() {
      let check = nvs.get_current_version();
      if let Ok(cur) = check {
        assert_eq!(cur, String::from("1.0.0"));
      }
    }
  }
}
