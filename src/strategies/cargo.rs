use crate::strategies::VersionStrategy;
use std::{
  fs::{read_to_string, File},
  io::Write,
  path::{Path, PathBuf},
};
use toml::{Table, Value};
pub struct CargoVerstionStrategy {
  pub cwd: String,
}

impl CargoVerstionStrategy {
  fn get_cargo_toml_path(&self) -> PathBuf {
    let fs_string = format!("{}/{}", self.cwd, "Cargo.toml");
    let p = PathBuf::from(fs_string.as_str());
    return p;
  }

  fn get_cargo_toml_contents(&self) -> Result<Table, std::io::Error> {
    let cargo_toml_path = self.get_cargo_toml_path();
    let string_contents_raw = read_to_string(cargo_toml_path)?;
    let string_content: Table =
      toml::from_str(string_contents_raw.as_str()).expect("should get this");

    return Ok(string_content);
  }
}

impl VersionStrategy for CargoVerstionStrategy {
  fn bump(&self, new_version: String) -> Result<(), std::io::Error> {
    let cargo_toml_path = self.get_cargo_toml_path();
    let cargo_toml_path_ref = cargo_toml_path.as_path();
    let string_contents_raw = read_to_string(cargo_toml_path_ref)?;
    let mut string_content: Table =
      toml::from_str(string_contents_raw.as_str()).expect("should get this");
    string_content["package"]["version"] = Value::String(new_version);
    let mut cargo_file = File::create(cargo_toml_path_ref).expect("Need file");
    return cargo_file.write_all(toml::to_string_pretty(&string_content).unwrap().as_bytes());
  }

  fn get_current_version(&self) -> Result<String, std::io::Error> {
    let string_content = self.get_cargo_toml_contents()?;

    if let Some(current_version) = string_content["package"]["version"].as_str() {
      return Ok(String::from(current_version));
    }

    return Ok(String::from("0.1.0"));
  }
}

mod test {
  use crate::strategies::{cargo::CargoVerstionStrategy, VersionStrategy};
  #[test]
  fn test_current_version() {
    // Placeholder
    let cvs = CargoVerstionStrategy {
      cwd: String::from("./tests/rust"),
    };

    if let Ok(current_version) = cvs.get_current_version() {
      assert_eq!(current_version, String::from("0.1.0"));
    } else {
      // TODO(jim): Necessary?
      assert!(false);
    }
  }

  #[test]
  fn test_bump_version() {
    let cvs = CargoVerstionStrategy {
      cwd: String::from("./tests/rust"),
    };

    if let Ok(_bumped) = cvs.bump(String::from("1.0.0")) {
      let check = cvs.get_current_version();
      if let Ok(cur) = check {
        assert_eq!(cur, String::from("1.0.0"));
      }
    }
  }
}
