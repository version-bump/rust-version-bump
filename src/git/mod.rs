use lazy_static::lazy_static;
use regex::{Match, Regex};
use std::fmt;
pub struct GitUrlParseError {
  reason: String,
}
impl fmt::Display for GitUrlParseError {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "Oh no, something bad went down")
  }
}

impl fmt::Debug for GitUrlParseError {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    return f
      .debug_struct("GitUrlParseError")
      .field("reason", &self.reason)
      .finish();
  }
}

pub struct GitUrl {
  pub proto: String,
  pub username: String,
  pub pass: String,
  pub host: String,
  pub port: u32,
  pub path: String,
}

impl fmt::Debug for GitUrl {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    return f
      .debug_struct("GitUrl")
      .field("host", &self.host)
      .field("port", &self.port)
      .field("proto", &self.proto)
      .field("path", &self.path)
      .field("username", &self.username)
      .finish();
  }
}
// TODO(jim): This doesn't appear to work with git+ssh URLs, so we might want to come back and figure out what that's about.
const GIT_REGEX_STR: &str = r"^(?:(?P<proto>\w+)://)?(?:(?P<username>\w+)(?::(?P<pass>.+))?@)?(?P<host>.+?)(?::(?P<port>\d+))?(:|/)(?P<path>.*?)(\.git)?$";

pub fn parse_git_url(url_str: &str) -> Result<GitUrl, GitUrlParseError> {
  lazy_static! {
    static ref REG: Regex = Regex::new(GIT_REGEX_STR).expect("Cant parse git regex");
  }
  let matches = REG.captures(url_str);

  if matches.is_none() {
    return Err(GitUrlParseError {
      reason: String::from("No matches"),
    });
  }

  if matches.is_some() {
    let match_ref = matches.as_ref().unwrap();
    let host = match_ref.name("host");
    let path = match_ref.name("path");
    if host.is_none() || path.is_none() {
      return Err(GitUrlParseError {
        reason: String::from("No host or path given"),
      });
    }
    // We want the port to be a number because like it has to
    // As an aside I have no idea how what I could possibly do to make this not a giant ball of fuck.
    let port = match_ref
      .name("port")
      .map(|f| f.as_str().parse::<u32>())
      .unwrap_or(Ok(443));

    let port_ref = port.as_ref();

    return Ok(GitUrl {
      host: String::from(host.unwrap().as_str()),
      port: *port_ref.ok().unwrap(),
      pass: String::from(match_ref.name("pass").map_or("", |m| m.as_str())),
      path: String::from(path.map_or("", |m| m.as_str())),
      proto: String::from(match_ref.name("proto").map_or("", |m| m.as_str())),
      username: String::from(match_ref.name("username").map_or("", |m| m.as_str())),
    });
  }
  return Ok(GitUrl {
    proto: String::from("ssh"),
    username: String::from("git"),
    pass: String::from(""),
    host: String::from("gitlab.com"),
    port: 443,
    path: String::from("jhechtf/version-bump"),
  });
}
